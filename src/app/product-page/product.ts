import {Brand} from './brand';
import {Price} from './price';

export class Product {
  id: number;
  shopId: number;
  name: string;
  description: string;
  path: string;
  balkone: boolean;
  balkoneCount: number;
  brand: Brand;
  price: Price;
  availability: object;
  styles: object[];
  new: boolean;
}
