import { Component, OnInit } from '@angular/core';
import {Product} from './product';
import {ProductService} from './product.service';
import {MatGridListModule} from '@angular/material/grid-list';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent implements OnInit {

  product: Product;

  constructor(private productService: ProductService) {
    this.product = productService.getProductById(1);
    console.log(this.product);
  }

  ngOnInit() {
  }

}
