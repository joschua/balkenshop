import { Injectable } from '@angular/core';
import ProductsJson from '../../assets/mock/product01.json';
import {Product} from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  productsJson;

  constructor() {
    this.productsJson = ProductsJson;
  }

  getProductById(id: number) {
    return this.productsJson.find(product => product.id === id) as Product;
  }
}
