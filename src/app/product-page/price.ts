export class Price {
  value: string;
  reduction: number;
  leckmichfett: boolean;
  uniquePrice: boolean;
  currency: object;
  formerPrice: Price;
  packItemsCount: number;
}
